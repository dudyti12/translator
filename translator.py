import speech_recognition as sr
import pydub
import srt
import datetime as dt



myaudio = intro = pydub.AudioSegment.from_wav ('a.wav')
bicara = pydub.silence.detect_nonsilent(myaudio,min_silence_len=500, silence_thresh=-25)
#min_silence_len = kecepatan berbicara, silence_thresh = volume suara (minus semakin sensitif)
bicara = [(int(start/1000),int(stop/1000)) for start,stop in bicara]

bicara_clean = []
skip= []
for idx,val in enumerate(bicara):
    if idx not in skip:
        if val[1]-val[0] < 1:
            try:
                bicara_clean.append(val[0], bicara[idx+1][1])
                skip.append(idx+1)
            except:
                bicara_clean.append(val)
        else:
            bicara_clean.append(val)
                    
print(bicara_clean)

engine = sr.Recognizer()
subs = []
timer = 0

with sr.AudioFile('a.wav') as source:
    for i,v in enumerate(bicara_clean):
        audiotext = engine.record(source, duration=v[1]-timer)
        
        try:
            kata = engine.recognize_google(audiotext, language = "en-US")
            subs.append(srt.Subtitle(index=(i+1),start=dt.timedelta(seconds=v[0]),end=dt.timedelta(seconds=v[1]), content=kata))
            timer += (v[1]-timer)
            print(v[1])
        except sr.UnknownValueError:
            print("Maaf Suara Tidak Bisa Terdeteksi")
        except Exception as e:
            print(e)
filesub = open("subtitle.srt", "w")
filesub.write(srt.compose(subs))
filesub.close

